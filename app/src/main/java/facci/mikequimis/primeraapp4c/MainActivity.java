package facci.mikequimis.primeraapp4c;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button buttonLogin, buttonRegistrar, buttonBuscar,botonParametro, botonfragmento, btnautenticar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonRegistrar = (Button) findViewById(R.id.buttonGuardar);
        buttonBuscar = (Button) findViewById(R.id.buttonBuscar);
        botonParametro = (Button) findViewById(R.id.btnapasarparametro);
        botonfragmento = (Button) findViewById(R.id.btnfragmentos);
        btnautenticar = (Button) findViewById(R.id.btnautenticar);

        btnautenticar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Autenticar.class);
                startActivity(intent);
            }
        });

        botonfragmento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, fragmentos.class);
                startActivity(intent);
            }
        });

        botonParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadPasarParametro.class);
                startActivity(intent);
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this,
                        Login.class);
                startActivity(intent);
            }
        });
        buttonRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this,
                        Registrar.class);
                startActivity(intent);
            }
        });
        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this,
                        Buscar.class);
                startActivity(intent);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent = new Intent(MainActivity.this, Login.class);
                startActivity(intent);
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionRegistrar:
                intent = new Intent(MainActivity.this, Registrar.class);
                startActivity(intent);
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionacelerometro:
                intent = new Intent(MainActivity.this, SensorAcelerometro.class);
                startActivity(intent);
                break;
        }
            switch (item.getItemId()){
                case R.id.opcionvibracion:
                    intent = new Intent(MainActivity.this, HardwareVibracion.class);
                    startActivity(intent);
                    break;
            }
                switch (item.getItemId()){
                    case R.id.opcionProximidad:
                        intent = new Intent(MainActivity.this, SensorProximidad.class);
                        startActivity(intent);
                        break;
                }
        switch (item.getItemId()){
            case R.id.opcionbrujula:
                intent = new Intent(MainActivity.this, SensorBrujula.class);
                startActivity(intent);
                break;

        }

        return true;
    }
        }


